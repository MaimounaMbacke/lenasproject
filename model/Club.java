package sn.iam.model;

import java.util.Set;



public class Club {
	


	private Long id;
	private String nom;
	private Set<Athlete> athletes;
	private Discipline disciplines;
	private Set<Performance>  performances;
	
	public Discipline getDisciplines() {
		return disciplines;
	}
	public void setDisciplines(Discipline disciplines) {
		this.disciplines = disciplines;
	}
	public Set<Athlete> getAthletes() {
		return athletes;
	}
	public void setAthletes(Set<Athlete> athletes) {
		this.athletes = athletes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	

}
