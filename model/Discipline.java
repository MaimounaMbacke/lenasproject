package sn.iam.model;




public class Discipline {


	private Long id;
	private String intitule;
	private String description;
	private Club club;

	public Club getClub() {
		return club;
	}

	public void setClubs(Club club) {
		this.club = club;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
