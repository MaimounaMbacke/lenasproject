package sn.iam.model;




public class Performance {

	private Long id;
	private String endurance;
	private String force;
	private String souplesse;
	private String vitesse;
	
	private Athlete  athlete;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEndurance() {
		return endurance;
	}
	public void setEndurance(String endurance) {
		this.endurance = endurance;
	}
	public String getForce() {
		return force;
	}
	public void setForce(String force) {
		this.force = force;
	}
	public String getSouplesse() {
		return souplesse;
	}
	public void setSouplesse(String souplesse) {
		this.souplesse = souplesse;
	}
	public String getVitesse() {
		return vitesse;
	}
	public void setVitesse(String vitesse) {
		this.vitesse = vitesse;
	}
	
}